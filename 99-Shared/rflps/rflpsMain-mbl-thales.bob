<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-09-26 11:39:45 by rafaelmontano-->
<display version="2.0.0">
  <name>rflpsMainHBL</name>
  <macros>
    <DEVICE>RFS-FIM-101</DEVICE>
  </macros>
  <width>2280</width>
  <height>1200</height>
  <background_color>
    <color name="Transparent" red="255" green="255" blue="255" alpha="0">
    </color>
  </background_color>
  <actions>
  </actions>
  <widget type="tabs" version="2.0.0">
    <name>Tabbed Container</name>
    <tabs>
      <tab>
        <name>   System  Overview</name>
        <children>
          <widget type="embedded" version="2.0.0">
            <name>Linking Container_1</name>
            <file>support/overview-mbl-thales.bob</file>
            <width>1965</width>
            <height>1085</height>
            <actions>
            </actions>
            <border_color>
              <color name="TEXT" red="25" green="25" blue="25">
              </color>
            </border_color>
          </widget>
        </children>
      </tab>
      <tab>
        <name>   SIM  Transitions</name>
        <children>
          <widget type="embedded" version="2.0.0">
            <name>Embedded Display</name>
            <file>support/transitions-mbl-thales.bob</file>
            <y>2</y>
            <width>1965</width>
            <height>1085</height>
          </widget>
        </children>
      </tab>
      <tab>
        <name>   FIM  Transitions</name>
        <children>
          <widget type="embedded" version="2.0.0">
            <name>transitions_fim</name>
            <file>support/transitions_fim.bob</file>
            <y>2</y>
            <width>1965</width>
            <height>1085</height>
          </widget>
        </children>
      </tab>
      <tab>
        <name>   Diagnostics</name>
        <children>
          <widget type="embedded" version="2.0.0">
            <name>Embedded Display_1</name>
            <file>support/rflpsDiagnostics-mbl.bob</file>
            <y>2</y>
            <width>1965</width>
            <height>1085</height>
          </widget>
        </children>
      </tab>
      <tab>
        <name>   Interlocks</name>
        <children>
          <widget type="embedded" version="2.0.0">
            <name>Embedded Display_1</name>
            <file>support/InterlocksMBL.bob</file>
            <y>2</y>
            <width>1965</width>
            <height>1085</height>
          </widget>
        </children>
      </tab>
    </tabs>
    <x>304</x>
    <y>59</y>
    <width>1970</width>
    <height>1121</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="21.0">
      </font>
    </font>
    <tab_height>10</tab_height>
    <actions>
    </actions>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>EmbeddedMachineControl</name>
    <file>support/machineStateControl-hbl.bob</file>
    <width>300</width>
    <height>1180</height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>$(PREFIX) - Klystron: $(KLY) - Thales</text>
    <x>314</x>
    <y>7</y>
    <width>486</width>
    <height>45</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="30.0">
      </font>
    </font>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <x>800</x>
    <width>220</width>
    <height>60</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>MGGrey03-background_6</name>
      <width>220</width>
      <height>60</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>8</corner_width>
      <corner_height>8</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_320</name>
      <text>FIM Reset:</text>
      <x>10</x>
      <y>15</y>
      <width>74</width>
      <height>30</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <actions>
      </actions>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Write PV</description>
        </action>
      </actions>
      <pv_name>$(PREFIX):$(R)Rst</pv_name>
      <text>FIM Reset</text>
      <x>100</x>
      <y>15</y>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group_1</name>
    <x>1040</x>
    <width>440</width>
    <height>60</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>MGGrey03-background_4</name>
      <width>440</width>
      <height>60</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>8</corner_width>
      <corner_height>8</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LGGrey02-title</name>
      <text>SIM OP Mode:</text>
      <x>20</x>
      <y>15</y>
      <width>99</width>
      <height>30</height>
      <font>
        <font name="SUBSUB-GROUP-HEADER" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <background_color>
        <color name="RED-BORDER" red="150" green="8" blue="16">
        </color>
      </background_color>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="combo" version="2.0.0">
      <name>Device prefix combo_4</name>
      <pv_name>$(PREFIX):$(R_SIM)OPMode</pv_name>
      <x>130</x>
      <y>15</y>
      <width>160</width>
      <actions>
      </actions>
      <tooltip>$(pv_name)
$(pv_value)
Change the Operation mode of the Amplifier</tooltip>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_36</name>
      <pv_name>$(PREFIX):$(R_SIM)OPMode-RB</pv_name>
      <x>300</x>
      <y>15</y>
      <width>120</width>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
      <actions>
      </actions>
      <rules>
        <rule name="opRuleColor" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0&lt;&gt;2">
            <value>
              <color name="MINOR" red="252" green="242" blue="17">
              </color>
            </value>
          </exp>
          <exp bool_exp="pv0==2">
            <value>
              <color name="OK" red="61" green="216" blue="61">
              </color>
            </value>
          </exp>
          <pv_name>$(pv_name)</pv_name>
        </rule>
      </rules>
      <tooltip>$(pv_name)
$(pv_value)
Operation Mode read-back</tooltip>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group_2</name>
    <x>1500</x>
    <width>410</width>
    <height>60</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>MGGrey03-background_3</name>
      <width>410</width>
      <height>60</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>8</corner_width>
      <corner_height>8</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_310</name>
      <text>Actual State:</text>
      <x>10</x>
      <y>15</y>
      <height>30</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_311</name>
      <class>SECTION</class>
      <text>SIM:</text>
      <x>120</x>
      <y>15</y>
      <width>40</width>
      <height>30</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <actions>
      </actions>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_123</name>
      <pv_name>$(PREFIX):$(R_SIM)ActualState-RB</pv_name>
      <x>171</x>
      <y>15</y>
      <width>80</width>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
      <actions>
      </actions>
      <tooltip>$(pv_name)
$(pv_value)
Actual Machine State read-back</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_312</name>
      <class>SECTION</class>
      <text>FIM:</text>
      <x>260</x>
      <y>15</y>
      <width>40</width>
      <height>30</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <actions>
      </actions>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_124</name>
      <pv_name>$(PREFIX):$(R)FSM-RB</pv_name>
      <x>310</x>
      <y>15</y>
      <width>80</width>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
      <actions>
      </actions>
      <tooltip>$(pv_name)
$(pv_value)
Actual Machine State read-back</tooltip>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group_3</name>
    <x>1930</x>
    <width>344</width>
    <height>60</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>MGGrey03-background_5</name>
      <width>344</width>
      <height>60</height>
      <line_width>2</line_width>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </background_color>
      <corner_width>8</corner_width>
      <corner_height>8</corner_height>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_128</name>
      <pv_name>$(PREFIX):$(R_SIM)WarnAckReq-RB</pv_name>
      <x>124</x>
      <y>15</y>
      <width>90</width>
      <height>30</height>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <rules>
        <rule name="alarmRule" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0==1">
            <value>
              <color name="MINOR" red="252" green="242" blue="17">
              </color>
            </value>
          </exp>
          <exp bool_exp="pv0==0">
            <value>
              <color name="OK" red="61" green="216" blue="61">
              </color>
            </value>
          </exp>
          <pv_name>$(pv_name)</pv_name>
        </rule>
      </rules>
      <tooltip>$(pv_name)
$(pv_value)
One or more signals are being interlocked or may require an acknowledge.</tooltip>
    </widget>
    <widget type="bool_button" version="2.0.0">
      <name>RESET_BUTTON_4</name>
      <pv_name>$(PREFIX):$(R_SIM)Reset</pv_name>
      <x>225</x>
      <y>15</y>
      <off_label>PLC Reset</off_label>
      <off_color>
        <color name="Background" red="220" green="225" blue="221">
        </color>
      </off_color>
      <on_label>Reseting...</on_label>
      <on_color>
        <color name="On" red="70" green="255" blue="70">
        </color>
      </on_color>
      <show_led>false</show_led>
      <background_color>
        <color red="240" green="240" blue="240">
        </color>
      </background_color>
      <actions>
      </actions>
      <tooltip>$(pv_name)
$(pv_value)
Reset all non-active interlocks</tooltip>
      <confirm_message>This will reset all non-active interlocks</confirm_message>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_319</name>
      <text>PLC Warning:</text>
      <x>14</x>
      <y>15</y>
      <height>30</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <actions>
      </actions>
    </widget>
  </widget>
</display>
